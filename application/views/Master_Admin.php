						<?php
						$data = $this;
						$this->load->view('Templates/Header', $data, false);

						?>

						<!-- ============================================================== -->
						<!-- Start Page Content here -->
						<!-- ============================================================== -->

						<div class="content-page">
							<div class="content">
								<div class="clearfix"></div>

								<!-- Start Content-->
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">
											<div class="card">
												<div class="card-body">
													<h4 class="header-title">Master Admin</h4>
													<button class="btn btn-primary" id="tambahAdmin" type="button">Tambah Admin</button>
													<table id="datatableUSer" class="table table-striped dt-responsive nowrap w-100">
														<thead>
															<tr>
																<th>Nomor</th>
																<th>Nama Admin</th>
																<th>Username</th>
																<th>Email</th>
																<th>Role</th>
																<th>Aksi</th>
															</tr>
														</thead>
														<tbody>

														</tbody>
													</table>

												</div> <!-- end card body-->
											</div> <!-- end card -->
										</div><!-- end col-->
									</div>
									<!-- end row-->
								</div> <!-- container -->
							</div> <!-- content -->
						</div>

						<div id="user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="JudulEditAdmin">Edit Admin</h5>
										<h5 class="modal-title" id="JudulTambahAdmin">Tambah Admin</h5>
										<button class="close" data-dismiss="modal" aria-label="Close">
											<a href="#" class="closeModal" data-dismiss="modal" aria-label="close">&times;</a>
										</button>
									</div>
									<div class="modal-body">
										<div class="hidden">
											<input type="hidden" id="id_admin" name="id_admin" value="">
										</div>
										<div class="form-group">
											<label for="">Nama Admin</label>
											<input type="text" class="form-control" id="nama_admin" placeholder="Input field">
										</div>
										<div class="form-group">
											<label for="">Username</label>
											<input type="text" class="form-control" id="username" placeholder="Input field">
										</div>
										<div class="form-group">
											<label for="">Email</label>
											<input type="text" class="form-control" id="email" placeholder="Input field">
										</div>
										<div class="form-group">
											<label for="">No Phone</label>
											<input type="text" class="form-control" id="no_phone" placeholder="Input field">
										</div>
										<div class="form-group">
											<label for="">Password</label>
											<input type="text" class="form-control" id="password" placeholder="Input field">
										</div>
										<div class="form-group">
											<label for="">Role Admin</label>
											<select name="role" id="role_admin" class="form-control" required="required">
												<option value="1">Role 1</option>
												<option value="2">Role 2</option>
											</select>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" id="SimpanTambah" type="button">Simpan</button>
										<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button>
									</div>
								</div>
							</div>
						</div>

						<script>
							$(document).ready(function() {
								// $('#user').modal('show');
								var bu = '<?= base_url(); ?>';
								var ba = '<?= base_url(); ?>Admin';
								$('.closeModal').click(function(e) {
									$("#user").modal("hide");
								});
								$('#SimpanEditUser').click(function(e) {

									var id_admin = $('#id_admin').val();
									var nama_admin = $('#nama_admin').val();
									var username = $('#username').val();
									var email = $('#email').val();
									var no_phone = $('#no_phone').val();
									var password = $('#password').val();
									var role = $('#role').val();
									$.ajax({
										type: "post",
										url: ba + '/EditAdmin',
										data: {
											id_admin,
											nama_admin,
											email,
											no_phone,
											password,
											role,
											username
										},
										dataType: "json",
										success: function(r) {
											$("#user").modal("hide");
											console.log("reload");
											table.ajax.reload()
											
											if (r.status) {
												Swal.fire(
													'Berhasil',
													r.message,
													'success'
												)
											} else {
												Swal.fire(
													'Gagal',
													r.message,
													'error'
												)
											}
										}
									});

								});
								var table = $('#datatableUSer').DataTable({
									'lengthMenu': [
										[5, 10, 25, 50, 100],
										[5, 10, 25, 50, 100]
									],
									'pageLength': 10,
									dom: 'Blfrtip',
									"type": "POST",
									"processing": true,
									"serverSide": true,
									"language": {
										processing: '....loading<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>loading....<span class="sr-only">Loading...</span> '
									},
									"order": [],
									ajax: {
										url: bu + "Admin/GetDataAdmin",
										type: 'POST',
										"data": function(d) {
											d.status = $('#Status').children('option:selected').val();
											d.tipe = $('#Tipe').children('option:selected').val();
											d.userHapus = $('#userHapus').children('option:selected').val();

											return d;
										},
										"orderable": false,

									},

									"columnDefs": [{
										"targets": [0, 4],
										"className": "dt-body-center dt-head-center",
										"width": "20px",
										"orderable": false,
									}, ],
									buttons: [{
											title: 'Report Admin',
											extend: 'excelHtml5',
											exportOptions: {
												columns: [0, 1, 2, 3,4]
											}
										},

									]
								});

								$('body').on('click', '.Hapus', function() {

									var nama_admin = $(this).data("nama_admin")
									var id_admin = $(this).data("id_admin")

									Swal.fire({
										title: 'Apakah Anda Yakin Akan Menghapus User : ' + nama_admin,
										text: 'Confirm Jika Iya',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Confirm'
									}).then((result) => {
										if (result.isConfirmed) {
											$.ajax({
												type: "post",
												url: bu + 'Admin/HapusAdmin',
												data: {
													id_admin
												},
												dataType: "json",
												success: function(r) {

													table.ajax.reload()
													if(r.status){
														Swal.fire(
														  'Berhasil',
														  r.message,
														  'success'
														)
													}else{
														Swal.fire(
														  'Maaf',
														  r.message,
														  'error'
														)
													}

												}
											});
										}
									})

								});
								$('body').on('click', '.Edit', function() {
									$('#user').modal('show');
									$('#SimpanTambah').hide();
									$('#JudulTambahAdmin').hide();
									$('#SimpanEditUser').show();
									$('#SimpanEditUser').show();

									var id_admin = $(this).data("id_admin")
									var nama_admin = $(this).data("nama_admin")
									var username = $(this).data("username")
									var email = $(this).data("email")
									var no_phone = $(this).data("no_phone")
									var password = $(this).data("password")
									$('#id_admin').val(id_admin);
									$('#username').val(username);
									$('#nama_admin').val(nama_admin);
									$('#email').val(email);
									$('#no_phone').val(no_phone);
									$('#password').val(password);

								});
								$('#SimpanTambah').click(function(e) {
									var nama_admin = $('#nama_admin').val();
									var email = $('#email').val();
									var username = $('#username').val();
									var no_phone = $('#no_phone').val();
									var password = $('#password').val();
									var role_admin = $('#role_admin').val();
									if (nama_admin == '' || email == '' || username == '' || no_phone == '' || password == '' || role_admin == '') {
										Swal.fire(
											'Mohon Lengkapi Data ',
											'Yang DiButuhkan',
											'error'
										)
									} else {
										$.ajax({
											type: "post",
											url: ba + '/tambah_admin',
											data: {
												nama_admin,
												email,
												username,
												no_phone,
												password,
												role_admin
											},
											dataType: "json",
											success: function(r) {
												console.log("reload");
											table.ajax.reload()
												$('#user').modal('hide');

												if(r.status){
													Swal.fire(
													  'Berhasil',
													  r.message,
													  'success'
													)
												}else{
													Swal.fire(
													  'Maaf',
													  'Gagal , Atau Data Sudah Pernah Ada!',
													  'error'
													)
												}
											}
										});
									}

								});
								
								$('#tambahAdmin').click(function (e) { 									
									$('#user').modal('show');
									$('#SimpanTambah').show();
									$('#JudulTambahAdmin').show();
									$('#SimpanEditUser').hide();
									$('#SimpanEditUser').hide();

								});


							});
						</script>

						<!-- END wrapper -->
						<?php
						$this->load->view('Templates/Footer', $data, false);

						?>
