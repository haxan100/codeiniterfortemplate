

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/vendor.min.js"></script>

        <!-- Plugins js-->
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/apexcharts/apexcharts.min.js"></script>

        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/selectize/js/standalone/selectize.min.js"></script>

        <!-- Dashboar 1 init js-->
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/pages/dashboard-1.init.js"></script>

        <!-- App js-->
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/app.min.js"></script>

		     <!-- third party js -->
		<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/vfs_fonts.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



        <!-- third party js ends -->

        <!-- Datatables init -->
        <!-- <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/pages/datatables.init.js"></script> -->

        <!-- App js -->
        <!-- <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/app.min.js"></script> -->


    </body>
    <script>
        $(document).ready(function () {
            $('#tombolLogout').click(function (e) { 
                e.preventDefault();
                Swal.fire({
                  title: 'Apakah Anda Yakin ',
                  text: 'Akan Logout?',
                  icon: 'question',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Logout!'
                }).then((result) => {
                  if (result.isConfirmed) {
                      $.ajax({
                          type: "post",
                          url: "<?= base_url() ?>/Admin/logout",
                          data: "data",
                          dataType: "json",
                          success: function (r) {
                              console.log(r);
                              Swal.fire(
                                'Berhasil',
                                r.message,
                                'success'
                              )
                            window.location.href= '<?= base_url() ?>/Admin/login'


                              
                          }
                      });
                  }
                })
                
            });
        });
    </script>
</html>
