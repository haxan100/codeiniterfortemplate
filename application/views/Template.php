		<?php
        $data = $this;
        $this->load->view('Templates/Header', $data, FALSE);
        ?>

		<!-- ============================================================== -->
		<!-- Start Page Content here -->
		<!-- ============================================================== -->

		<div class="content-page">
		    <div class="content">

		        <!-- Start Content-->
		        <div class="container-fluid">

		            <!-- start page title -->
		            <div class="row">
		                <div class="col-12">
		                    <div class="page-title-box">
		                        <h4 class="page-title">Dashboard</h4>
		                    </div>
		                </div>
		            </div>
		            <!-- end page title -->

		            <div class="row">

		                <a class="col-md-4 col-xl-4" href="<?= base_url() ?>Admin/master_user">
		                    <div class="widget-rounded-circle card">
		                        <div class="card-body">
		                            <div class="row">
		                                <div class="col-6">
		                                    <div class="avatar-lg rounded-circle bg-soft-success border-success border">
		                                        <i class="fe-users font-22 avatar-title text-success"></i>
		                                    </div>
		                                </div>
		                                <div class="col-6">
		                                    <div class="text-end">
		                                        <h3 class="text-dark mt-1"><span data-plugin="counterup"><?= count($dataUser); ?></span></h3>
		                                        <p class="text-muted mb-1 text-truncate">User's</p>
		                                    </div>
		                                </div>
		                            </div> <!-- end row-->
		                        </div>
		                    </div> <!-- end widget-rounded-circle-->
		                </a> <!-- end col-->

		                <div class="col-md-4 col-xl-4">
		                    <div class="widget-rounded-circle card">
		                        <div class="card-body">
		                            <div class="row">
		                                <div class="col-6">
		                                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
		                                        <i class="fe-bar-chart-line- font-22 avatar-title text-info"></i>
		                                    </div>
		                                </div>
		                                <div class="col-6">
		                                    <div class="text-end">
		                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">0.58</span></h3>
		                                        <p class="text-muted mb-1 text-truncate">Transaction</p>
		                                    </div>
		                                </div>
		                            </div> <!-- end row-->
		                        </div>
		                    </div> <!-- end widget-rounded-circle-->
		                </div> <!-- end col-->

		                <div class="col-md-4 col-xl-4">
		                    <div class="widget-rounded-circle card">
		                        <div class="card-body">
		                            <div class="row">
		                                <div class="col-6">
		                                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
		                                        <i class="fe-eye font-22 avatar-title text-warning"></i>
		                                    </div>
		                                </div>
		                                <div class="col-6">
		                                    <div class="text-end">
		                                        <h3 class="text-dark mt-1"><span data-plugin="counterup">78.41</span>k</h3>
		                                        <p class="text-muted mb-1 text-truncate">Data</p>
		                                    </div>
		                                </div>
		                            </div> <!-- end row-->
		                        </div>
		                    </div> <!-- end widget-rounded-circle-->
		                </div> <!-- end col-->
		            </div>
		            <!-- end row-->
		            <div class="container">
		                <div class="card">
		                    <img class="card-img-top" src="holder.js/100x180/" alt="">
		                    <div class="card-body">
		                        <h4 class="card-title text-center">Selamat Datang Admin</h4>
		                    </div>
		                </div>


		            </div>


		        </div> <!-- container -->

		    </div> <!-- content -->


		</div>

		<!-- ============================================================== -->
		<!-- End Page content -->
		<!-- ============================================================== -->


		</div>
		<!-- END wrapper -->
		<?php
        $this->load->view('Templates/Footer', $data, FALSE);

        ?>