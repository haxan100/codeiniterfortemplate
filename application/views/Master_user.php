						<?php
$data = $this;
$this->load->view('Templates/Header', $data, false);

?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
							<div class="content">
									<div class="clearfix"></div>

                    <!-- Start Content-->
                    <div class="container-fluid">
											<div class="row">
													<div class="col-12">
															<div class="card">
																	<div class="card-body">
																			<h4 class="header-title">Master User</h4>
																			<!-- <button class="btn btn-primary" type="button">Tambah User</button> -->
																			<table id="datatableUSer" class="table table-striped dt-responsive nowrap w-100">
																					<thead>
																							<tr>
																									<th>Nomor</th>
																									<th>Nama User</th>
																									<th>Email</th>
																									<th>Nomor Telpon</th>
																									<th>Aksi</th>
																							</tr>
																					</thead>
																					<tbody>

																					</tbody>
																			</table>

																	</div> <!-- end card body-->
															</div> <!-- end card -->
													</div><!-- end col-->
											</div>
                        <!-- end row-->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>

						<div id="user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="EditUser">Edit User</h5>
										<button class="close" data-dismiss="modal" aria-label="Close">
										<a href="#" class="closeModal" data-dismiss="modal" aria-label="close">&times;</a>
										</button>
									</div>
									<div class="modal-body">
										<div class="hidden">
											<input type="hidden" id="id_user" name="id_user" value="">
										</div>
											<div class="form-group">
												<label for="">Nama User</label>
												<input type="text" class="form-control" id="nama_user" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">Email</label>
												<input type="text" class="form-control" id="email" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">No Phone</label>
												<input type="text" class="form-control" id="no_phone" placeholder="Input field">
											</div>



									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button>
									</div>
								</div>
							</div>
						</div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

						<script>
							$(document).ready(function () {
								var bu = '<?=base_url();?>';
								var ba = '<?=base_url();?>Admin';
								$('.closeModal').click(function (e) {
									$("#user").modal("hide");
								});
								$('#SimpanEditUser').click(function (e) {

									var id_user = $('#id_user').val();
									var nama_user = $('#nama_user').val();
									var email = $('#email').val();
									var no_phone = $('#no_phone').val();
									$.ajax({
										type: "post",
										url: ba+'/EditUser',
										data: {
											id_user,
											nama_user,
											email,
											no_phone
										},
										dataType: "json",
										success: function (r) {
										$("#user").modal("hide");
										table.ajax.reload()
											if(r.status){
												Swal.fire(
													'Berhasil',
													r.message,
													'success'
												)
											}else{
												Swal.fire(
													'Gagal',
													r.message,
													'error'
												)
											}
										}
									});

								});
								var table= 	$('#datatableUSer').DataTable( {
									'lengthMenu': [
										[5, 10, 25, 50, 100],
										[5, 10, 25, 50, 100]
									],
									'pageLength': 10,
									dom: 'Blfrtip',
									"type": "POST",
									"processing": true,
									"serverSide": true,
									"language": {
										processing: '....loading<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>loading....<span class="sr-only">Loading...</span> '
									},
									"order": [],
									ajax:  {
										url: bu+"User/GetDataUser/",
										type: 'POST',
										"data": function(d) {
											d.status = $('#Status').children('option:selected').val();
											d.tipe = $('#Tipe').children('option:selected').val();
											d.userHapus = $('#userHapus').children('option:selected').val();

											return d;
										},
										"orderable": false,

									},

								"columnDefs": [{
										"targets": [0,4],
										"className": "dt-body-center dt-head-center",
										"width": "20px",
										"orderable": false,
									},
								],
									buttons: [
											{
													title: 'Report',
													extend: 'excelHtml5',
													exportOptions: {
														columns: [ 0, 1, 2,3 ]
													}
												},
									// 			{
									// 				extend: 'pdfHtml5',
									// 				title: 'Report',
									// 				className: "btn",
									// 				text: "Export To PDF",
									// 				title: "Data Master User",
									// 				filename: "Data Master User",
									// 				exportOptions: {
									// 						columns: [ 0, 1, 2 ],
									// 						extend:"pdfHtml5",
									// 				},
									// 				customize: function(doc) {
									// 					doc.content[1].margin = [ 150, 0, 100, 0 ] //left, top, right, bottom
									// 				}
									// 		},
									]
								} );

								$('body').on('click', '.Hapus', function() {

									var nama_user =$(this).data("nama_user")
									var id_user =$(this).data("id_user")

									Swal.fire({
										title: 'Apakah Anda Yakin Akan Menghapus User : '+nama_user,
										text: 'Confirm Jika Iya',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Confirm'
									}).then((result) => {
										if (result.isConfirmed) {
											$.ajax({
												type: "post",
												url: bu+'Admin/HapusUser',
												data: {
													id_user
												},
												dataType: "json",
												success: function (r) {
														table.ajax.reload()
															if(r.status){
																Swal.fire(
																	'Berhasil',
																	r.message,
																	'success'
																)
															}else{
																Swal.fire(
																	'Gagal',
																	r.message,
																	'error'
																)
															}

												}
											});
										}
									})

								});
								$('body').on('click', '.Edit', function() {
									$('#user').modal('show');
									console.log($(this).data());
									var id_user =$(this).data("id_user")
									var nama_user =$(this).data("nama_user")
									var email =$(this).data("email")
									var no_phone =$(this).data("no_phone")
									console.log(id_user,nama_user,email,no_phone);
									$('#id_user').val(id_user);
									$('#nama_user').val(nama_user);
									$('#email').val(email);
									$('#no_phone').val(no_phone);

								});
							});
						</script>

        <!-- END wrapper -->
<?php
$this->load->view('Templates/Footer', $data, false);

?>
