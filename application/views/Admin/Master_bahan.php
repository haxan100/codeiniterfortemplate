						<?php
						$data = $this;
						$this->load->view('Templates/Header', $data, false);

						?>

						<div class="content-page">
							<div class="content">
								<div class="clearfix"></div>
								<!-- Start Content-->
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">
											<div class="card">
												<div class="card-body">
													<h4 class="header-title">Master Bahan </h4>
													<br>
													<button class="btn btn-primary" id="tambahAdmin" type="button">Tambah Bahan</button>
													<br>
													<br>
													<div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
														<div class=" col-4 form-group ">
															<label for="">Jenis Bahan</label>
															<select class="form-control btn btn-primary m-t-20 btn-info waves-effect waves-light" name="jenisBahanFilter" id="jenisBahanFilter">
																<option value="default">Pilih Jenis Bahan</option>
																<option value="hijab">Hijab</option>
																<option value="kain">Kain</option>
																<option value="tshirt">T-Shirt</option>
																<option value="Totebag">Totebag</option>
															</select>
														</div>
													</div>
													<br>
													<table id="datatableUSer" class="table table-striped dt-responsive nowrap w-100">
														<thead>
															<tr>
																<th>Nomor</th>
																<th>Nama Bahan</th>
																<th>Jenis Bahan </th>
																<th>Aksi</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>

												</div> <!-- end card body-->
											</div> <!-- end card -->
										</div><!-- end col-->
									</div>
									<!-- end row-->
								</div> <!-- container -->
							</div> <!-- content -->
						</div>

						<div id="user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
							<form id="form">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="JudulEditAdmin">Edit Bahan</h5>
											<h5 class="modal-title" id="JudulTambahAdmin">Tambah Bahan</h5>
											<button class="close" type="button" data-dismiss="modal" aria-label="Close">
												<a href="#" class="closeModal" data-dismiss="modal" aria-label="close">&times;</a>
											</button>
										</div>
										<div class="modal-body">
											<div class="hidden">
												<input type="hidden" id="id_bahan" name="id_bahan" value="">
											</div>
											<div class="form-group">
												<label for="">Nama Bahan</label>
												<input type="text" class="form-control" id="nama_bahan" name="nama_bahan" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">Jenis Bahan </label>
												<select class="form-control" name="jenis" id="jenis">
													<option value="hijab">Hijab</option>
													<option value="kain">Kain</option>
													<option value="tshirt">T-Shirt</option>
													<option value="Totebag">Totebag</option>
												</select>
											</div>
											<br>
										</div>
										<div class="modal-footer">
											<button class="btn btn-primary" id="SimpanTambah" type="button">Simpan</button>

											<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button>
											<!-- <button class="btn btn-primary" id="SimpanTambah" type="button">Simpan</button>
										<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button> -->
										</div>
									</div>
								</div>
							</form>
						</div>

						<script>
							$(document).ready(function() {
								// $("#user").modal("show");

								var bu = '<?= base_url(); ?>';
								var ba = '<?= base_url(); ?>Admin';
								var url_form = bu + 'admin/addBahan';
								var url_form_ubah = bu + 'admin/editBahan';
								$('.closeModal').click(function(e) {
									$("#user").modal("hide");
								});
								$('#SimpanEditUser').click(function(e) {
									var nama_bahan = $('#nama_bahan').val();
									var jenis = $('#jenis').val();
									if (nama_bahan == '' || jenis == '') {
										Swal.fire(
											'Mohon Lengkapi Data ',
											'Yang DiButuhkan',
											'error'
										)
									} else {
										$("#form").submit();
									}
								});
								var table = $('#datatableUSer').DataTable({
									'lengthMenu': [
										[5, 10, 25, 50, 100],
										[5, 10, 25, 50, 100]
									],
									'pageLength': 10,
									dom: 'Blfrtip',
									"type": "POST",
									"processing": true,
									"serverSide": true,
									"language": {
										processing: '....loading<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>loading....<span class="sr-only">Loading...</span> '
									},
									"order": [],
									ajax: {
										url: bu + "Admin/GetBahan",
										type: 'POST',
										"data": function(d) {
											d.jenis = $('#jenisBahanFilter').val();
											return d;
										},
										"orderable": false,

									},

									"columnDefs": [{
										"targets": [0, 3],
										"className": "dt-body-center dt-head-center",
										"width": "20px",
										"orderable": false,
									}, ],
									buttons: [{
											title: 'Report Admin',
											extend: 'excelHtml5',
											exportOptions: {
												columns: [0, 1, 2]
											}
										},

									]
								});

								$('body').on('click', '.Hapus', function() {

									var nama_bahan = $(this).data("nama_bahan")
									var id_bahan = $(this).data("id_bahan")
									var jenis = $(this).data("jenis")

									Swal.fire({
										title: 'Apakah Anda Yakin Akan Menghapus Bahan : ' + nama_bahan + ', <br> Berjenis : ' + jenis,
										text: 'Confirm Jika Iya',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Confirm'
									}).then((result) => {
										if (result.isConfirmed) {
											$.ajax({
												type: "post",
												url: bu + 'Admin/HapusBahan',
												data: {
													id_bahan
												},
												dataType: "json",
												success: function(r) {
													table.ajax.reload()
													if (r.status) {
														Swal.fire(
															'Berhasil',
															r.message,
															'success'
														)
													} else {
														Swal.fire(
															'Maaf',
															r.message,
															'error'
														)
													}

												}
											});
										}
									})

								});
								$('body').on('click', '.Edit', function() {
									$('#user').modal('show');
									$('#SimpanTambah').hide();
									$('#JudulTambahAdmin').hide();
									$('#SimpanEditUser').show();
									$('#SimpanEditUser').show();
									url_form = url_form_ubah;

									var id_bahan = $(this).data("id_bahan")
									var nama_bahan = $(this).data("nama_bahan")
									var jenis = $(this).data("jenis")
									$('#id_bahan').val(id_bahan);
									$('#nama_bahan').val(nama_bahan);
									$('#jenis').val(jenis);


								});

								$('#SimpanTambah').click(function(e) {
									url_form = url_form;
									var bahan = $('#bahan').val();
									var jenis_bahan = $('#jenis_bahan').val();
									if (bahan == '' || jenis_bahan == '') {
										Swal.fire(
											'Mohon Lengkapi Data ',
											'Yang DiButuhkan',
											'error'
										)
									} else {
										$("#form").submit();
									}

								});

								$('#tambahAdmin').click(function(e) {
									$('#user').modal('show');
									$('#SimpanTambah').show();
									$('#JudulTambahAdmin').show();
									$('#SimpanEditUser').hide();
									$('#SimpanEditUser').hide();

								});

								$("#form").submit(function(e) {
									$.ajax({
										url: url_form,
										method: 'post',
										dataType: 'json',
										data: new FormData(this),
										processData: false,
										contentType: false,
										cache: false,
										async: false,
									}).done(function(e) {
										console.log("data Disimpan");
										console.log(e);
										if (e.status) {
											Swal.fire(
												'Berhasil',
												e.message,
												'success'
											)
											$('#user').modal('hide');
											table.ajax.reload();
											resetForm();
										} else {
											Swal.fire(
												'Maaf',
												e.message,
												'error'
											)
											$('#user').modal('hide');
										}
									}).fail(function(e) {
										Swal.fire(
											'Maaf',
											e.message,
											'error'
										)
										$('#user').modal('hide');
										table.ajax.reload();
										resetForm();
									});
									return false;
								});

								function resetForm() {
									$('#form').trigger('reset');
								}
								$('#jenisBahanFilter').change(function(e) {
									e.preventDefault();
									table.ajax.reload();

								});

							});
						</script>

						<!-- END wrapper -->
						<?php
						$this->load->view('Templates/Footer', $data, false);

						?>