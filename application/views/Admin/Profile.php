<?php
$data = $this;
$this->load->view('Templates/Header', $data, false);

?>

						<!-- ============================================================== -->
						<!-- Start Page Content here -->
						<!-- ============================================================== -->

						<div class="content-page">
							<div class="content">
								<div class="clearfix"></div>
								<div class="row">

									<div class="col-lg-12 col-xl-12">
											<div class="col-lg">
													<div class="card text-center">
															<div class="card-body">
																	<img src="<?=base_url()?>/Images/foto_admin/<?=$dataAdmin->foto_admin?>" class="rounded-circle avatar-lg img-thumbnail"
																	alt="profile-image">
																	<br>
																	<ul class="nav nav-pills nav-fill navtab-bg">
																		<li class="nav-item">
																				<a href=""  data-bs-toggle="tab" aria-expanded="true" class="nav-link active ubahFoto" >
																						Edit Gambar
																				</a>
																		</li>
																</ul>
																<br>

																	<h4 class="mb-0"><?=$dataAdmin->nama_admin?></h4>
															</div>
													</div> <!-- end card -->
											</div> <!-- end col-->
												<div class="card">
														<div class="card-body">

																<ul class="nav nav-pills nav-fill navtab-bg">
																		<li class="nav-item">
																				<a href="#settings"  data-bs-toggle="tab" aria-expanded="true" class="nav-link active ubahPassword" >
																						Edit Password
																				</a>
																		</li>
																</ul>
																<div class="tab-content">
																		<div class="tab-pane show active" id="settings" >
																						<h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle me-1"></i> Personal Info</h5>
																						<div class="row">
																								<div class="col-md-12">
																										<div class="mb-3">
																												<label for="firstname" class="form-label"> Name</label>
																												<input type="text" class="form-control" id="nama_admin" placeholder="Enter first name" value="<?=$dataAdmin->nama_admin?>" >
																										</div>
																								</div>
																						</div> <!-- end row -->
																						<div class="row">
																								<div class="col-md-12">
																										<div class="mb-3">
																												<label for="useremail" class="form-label">Email Address</label>
																												<input type="email" class="form-control" id="email" placeholder="Enter email" value="<?=$dataAdmin->email?>">
																										</div>
																								</div>
																								<div class="col-md-12">
																										<div class="mb-3">
																												<label for="useremail" class="form-label">No Phone</label></label>
																												<input type="number" class="form-control" id="no_phone" placeholder="Enter email" value="<?=$dataAdmin->no_phone?>">
																										</div>
																								</div>
																						</div> <!-- end row -->

																						<div class="text-end">
																								<button type="text" id="simpan" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
																						</div>
																		</div>
																		<!-- end settings content-->

																</div> <!-- end tab-content -->
														</div>
												</div> <!-- end card-->

										</div> <!-- end col -->
								</div>
							</div> <!-- content -->
						</div>
						<div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-body">
										<p>Ubah Password</p>
											<div class="form-group">
												<label for="">Password Baru</label>
												<input type="text" class="form-control" id="password" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">Password Baru Confirm</label>
												<input type="text" class="form-control" id="re_password" placeholder="Input field">
											</div>
											<br>
											<button type="#" id="UbahPassword" class="btn btn-primary">Simpan</button>
									</div>
								</div>
							</div>
						</div>
						<link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>
							<script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js' type='text/javascript'></script>

						<div id="fotoModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-body">

												<style>
												.dz-message{
													text-align: center;
													font-size: 28px;
												}
												</style>
												<script>
												// Add restrictions
												Dropzone.options.submit = {
													acceptedFiles: 'image/*',
													maxFilesize: 1,
													maxFiles: 1, 
													success: function(file, r) {
														Swal.fire(
																'Berhasil',
																'Melakukan Perubahan',
																'success'
															)
														setTimeout(() => {
															location.reload();
														}, 1500);
													$('#fotoModal').modal('hide');

													},

												error: function(file, response) {
																					alert(response);
															}
												};
									
												</script>
											</head>
											<body>

												<div class='content'>
													<!-- Dropzone -->
													<!-- <form enctype="multipart/form-data" id="submit" class="dropzone"> -->
													<form action="<?=base_url()?>/Admin/updateFotoProfile" class="dropzone" id="submit" enctype="multipart/form-data">

													<!-- <button type="submit" class="btn btn-primary">Submit</button> -->

												</form>
												</div>
									</div>
								</div>
							</div>
						</div>
						
						<script>
							$(document).ready(function() {

								// $('#user').modal('show');
								$('.ubahFoto').click(function (e) {
									e.preventDefault();
									$('#fotoModal').modal('show');

								});

								$('#fileupload').submit(function(e){
									e.preventDefault();
											$.ajax({
													url:'<?=base_url()?>/Admin/updateFotoProfile',
													type:"post",
													data:new FormData(this),
													processData:false,
													contentType:false,
													cache:false,
													async:false,
														success: function(data){
																// alert(data);
																console.log(data);
												}
											});
									});


								var bu = '<?=base_url();?>';
								var ba = '<?=base_url();?>Admin';
								$('.ubahPassword').click(function (e) {
									e.preventDefault();
									$('#my-modal').modal('show');

								});
								$('#UbahPassword').click(function (e) {

									var password = $('#password').val();
									var re_password = $('#re_password').val();

									if(password!=re_password){
										Swal.fire(
											'Maaf',
											'Harap Samakan Passwordnya!',
											'error'
										)
										return false
									}

									Swal.fire({
										title: 'Apakah Anda Yakin',
										text: 'Akan Mengubah Password?',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Ya, Ubah'
									}).then((result) => {
										if (result.isConfirmed) {

											$.ajax({
												type: "post",
												url: "<?=base_url()?>/Admin/EditPassword",
												data: {
													password,re_password
												},
												dataType: "json",
												success: function (r) {
													$('#my-modal').modal('hide');

													if(r.status){

														Swal.fire(
														'Berhasil',
														r.message,
														'success'
													)
													setTimeout(() => {
														location.reload();
													}, 1500);
													}else{
													Swal.fire(
														'Maaf',
														r.message,
														'error'
													)
												}

												}
											});
											//success
										}
									})


								});

								$('#simpan').click(function(e) {
									var nama_admin = $('#nama_admin').val();
									var email = $('#email').val();
									var no_phone = $('#no_phone').val();

									Swal.fire({
										title: 'Apakah Anda Yakin',
										text: 'Akan Mengubah Data',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Ya, Ubah'
									}).then((result) => {
										if (result.isConfirmed) {
											$.ajax({
												type: "post",
												url: "<?=base_url()?>/Admin/UpdateProfile",
												data: {
													nama_admin,email,no_phone
												},
												dataType: "json",
												success: function (r) {
													if(r.status){

														Swal.fire(
														'Berhasil',
														r.message,
														'success'
													)
													setTimeout(() => {
														location.reload();
													}, 1500);
													}else{
													Swal.fire(
														'Maaf',
														r.message,
														'error'
													)
												}

												}
											});
											//success
										}
									})


								});


							});
						</script>

						<!-- END wrapper -->
						<?php
$this->load->view('Templates/Footer', $data, false);

?>
