						<?php
						$data = $this;
						$this->load->view('Templates/Header', $data, false);

						?>

						<!-- ============================================================== -->
						<!-- Start Page Content here -->
						<!-- ============================================================== -->

						<div class="content-page">
							<div class="content">
								<div class="clearfix"></div>

								<!-- Start Content-->
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">
											<div class="card">
												<div class="card-body">
													<h4 class="header-title">Master Banner</h4>
													<button class="btn btn-primary" id="tambahAdmin" type="button">Tambah Banner</button>
													<table id="datatableUSer" class="table table-striped dt-responsive nowrap w-100">
														<thead>
															<tr>
																<th>Nomor</th>
																<th>Nama Judul</th>
																<th>Sub Judul</th>
																<th>Deskripsi</th>
																<th>Status</th>
																<th>Foto</th>
																<th>Aksi</th>
															</tr>
														</thead>
														<tbody>

														</tbody>
													</table>

												</div> <!-- end card body-->
											</div> <!-- end card -->
										</div><!-- end col-->
									</div>
									<!-- end row-->
								</div> <!-- container -->
							</div> <!-- content -->
						</div>

						<div id="user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
							<form id="form">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="JudulEditAdmin">Edit Banner</h5>
											<h5 class="modal-title" id="JudulTambahAdmin">Tambah Banner</h5>
											<button class="close" data-dismiss="modal" aria-label="Close">
												<a href="#" class="closeModal" data-dismiss="modal" aria-label="close">&times;</a>
											</button>
										</div>
										<div class="modal-body">
											<div class="hidden">
												<input type="hidden" id="id_banner" name="id_banner" value="">
											</div>
											<div class="form-group">
												<label for="">Judul Banner</label>
												<input type="text" class="form-control" id="judul" name="judul" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">Sub Judul</label>
												<input type="text" class="form-control" id="sub_judul" name="sub_judul" placeholder="Input field">
											</div>
											<div class="form-group">
												<label for="">Deskripsi</label>
												<textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="10"></textarea>
											</div>
											<div class="form-group">
												<label for="">Status</label>
												<select name="status" id="status" class="form-control" required="required">
													<option value="aktive">Aktive</option>
													<option value="draft">Draft</option>
												</select>
											</div>
											<br>
											<div class="form-group">
												<label for="">Gambar</label>
												<input type="file" class="form-control-file" name="gambar" id="gambar">
											</div>
										</div>
										<div class="modal-footer">
											<button class="btn btn-primary" id="SimpanTambah" type="button">Simpan</button>

											<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button>
											<!-- <button class="btn btn-primary" id="SimpanTambah" type="button">Simpan</button>
										<button class="btn btn-primary" id="SimpanEditUser" type="button">Edit</button> -->
										</div>
									</div>
								</div>
							</form>
						</div>

						<script>
							$(document).ready(function() {
								// $('#user').modal('show');
								var bu = '<?= base_url(); ?>';
								var ba = '<?= base_url(); ?>Admin';
								var url_form = bu + 'admin/addBanner';
								var url_form_ubah = bu + 'admin/editBanner';
								$('.closeModal').click(function(e) {
									$("#user").modal("hide");
								});
								$('#SimpanEditUser').click(function(e) {
									var judul = $('#judul').val();
									var sub_judul = $('#sub_judul').val();
									var deskripsi = $('#deskripsi').val();
									if (judul == '' || sub_judul == '' || deskripsi == '') {
										Swal.fire(
											'Mohon Lengkapi Data ',
											'Yang DiButuhkan',
											'error'
										)
									} else {
										$("#form").submit();	
									}
								});
								var table = $('#datatableUSer').DataTable({
									'lengthMenu': [
										[5, 10, 25, 50, 100],
										[5, 10, 25, 50, 100]
									],
									'pageLength': 10,
									dom: 'Blfrtip',
									"type": "POST",
									"processing": true,
									"serverSide": true,
									"language": {
										processing: '....loading<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>loading....<span class="sr-only">Loading...</span> '
									},
									"order": [],
									ajax: {
										url: bu + "Admin/GetBannerAdmin",
										type: 'POST',
										"data": function(d) {
											d.status = $('#Status').children('option:selected').val();
											d.tipe = $('#Tipe').children('option:selected').val();
											d.userHapus = $('#userHapus').children('option:selected').val();
											return d;
										},
										"orderable": false,
									},
									"columnDefs": [{
										"targets": [0, 6],
										"className": "dt-body-center dt-head-center",
										"width": "20px",
										"orderable": false,
									}, ],
									buttons: [{
											title: 'Report Banner',
											extend: 'excelHtml5',
											exportOptions: {
												columns: [0, 1, 2, 3, 4]
											}
										},

									]
								});

								$('body').on('click', '.Hapus', function() {

									var judul = $(this).data("judul")
									var id_banner = $(this).data("id_banner")

									Swal.fire({
										title: 'Apakah Anda Yakin Akan Menghapus User : ' + judul,
										text: 'Confirm Jika Iya',
										icon: 'question',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Confirm'
									}).then((result) => {
										if (result.isConfirmed) {
											$.ajax({
												type: "post",
												url: bu + 'Admin/HapusBanner',
												data: {
													id_banner
												},
												dataType: "json",
												success: function(r) {
													table.ajax.reload()
													if (r.status) {
														Swal.fire(
															'Berhasil',
															r.message,
															'success'
														)
													} else {
														Swal.fire(
															'Maaf',
															r.message,
															'error'
														)
													}

												}
											});
										}
									})

								});
								$('body').on('click', '.Edit', function() {
									$('#user').modal('show');
									$('#SimpanTambah').hide();
									$('#JudulTambahAdmin').hide();
									$('#SimpanEditUser').show();
									$('#SimpanEditUser').show();
									url_form = url_form_ubah;

									var id_banner = $(this).data("id_banner")
									$.ajax({
										type: "post",
										url: "<?= base_url() ?>/Admin/GetBannerByID",
										data: {
											id_banner
										},
										dataType: "json",
										success: function (r) {
											if(r.status==false){
												Swal.fire(
												  'Maaf',
												  r.message,
												  'error'
												)
											}else{
												$('#judul').val(r.data.judul);
												$('#id_banner').val(r.data.id_banner);
												$('#sub_judul').val(r.data.sub_judul);
												$('#deskripsi').val(r.data.deskripsi);
												$('#status').val(r.data.status);
											}
										}
									});

								});

								$('#SimpanTambah').click(function(e) {
									url_form = url_form;
									var judul = $('#judul').val();
									var sub_judul = $('#sub_judul').val();
									var deskripsi = $('#deskripsi').val();
									if (judul == '' || sub_judul == '' || deskripsi == '') {
										Swal.fire(
											'Mohon Lengkapi Data ',
											'Yang DiButuhkan',
											'error'
										)
									} else {
										$("#form").submit();	
									}

								});

								$('#tambahAdmin').click(function(e) {
									$('#user').modal('show');
									$('#SimpanTambah').show();
									$('#JudulTambahAdmin').show();
									$('#SimpanEditUser').hide();
									$('#SimpanEditUser').hide();

								});

								$("#form").submit(function(e) {
									$.ajax({
										url: url_form,
										method: 'post',
										dataType: 'json',
										data: new FormData(this),
										processData: false,
										contentType: false,
										cache: false,
										async: false,
									}).done(function(e) {
										console.log("data Disimpan");
										console.log(e);
										if (e.status) {
											Swal.fire(
											  'Berhasil',
											  e.message,
											  'success'
											)
											$('#user').modal('hide');
											table.ajax.reload();
											resetForm();
										} else {
											Swal.fire(
											  'Maaf',
											  e.message,
											  'error'
											)											
											$('#user').modal('hide');
										}
									}).fail(function(e) {
										Swal.fire(
											'Maaf',
											e.message,
											'error'
										)	
										$('#user').modal('hide');
										table.ajax.reload();
										resetForm();
									});
									return false;
								});
								function resetForm() {
									$('#form').trigger('reset');
								}

							});
						</script>

						<!-- END wrapper -->
						<?php
						$this->load->view('Templates/Footer', $data, false);

						?>