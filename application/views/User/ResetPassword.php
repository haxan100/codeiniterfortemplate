
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>/Assets/Admin/dist/assets/images/favicon.ico">

        <!-- Plugins css -->
        <link href="<?=base_url()?>/Assets/Admin/dist/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>/Assets/Admin/dist/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?=base_url()?>/Assets/Admin/dist/assets/css/config/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="<?=base_url()?>/Assets/Admin/dist/assets/css/config/default/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
		<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->

   
        <!-- icons -->
        <link href="<?=base_url()?>/Assets/Admin/dist/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <script src="<?=base_url()?>/Assets/Admin/dist/assets/js/pages/jquery-360.js"></script>

    </head> 
<title>
	GantiPassword
</title>
<div class="container">
	<div class="container-fluid">
			<div class="card shadow-sm">
				<div class="card-body">
		
					<div class="form-group">
						<div class="form-group">
							<label class="sr-only" for="newPassword">New Password</label>
							<div class="input-group">
								<input type="hidden" name="id_user" id="id_user" value="<?=$data['id_user']  ?>">		
								<input name="newPassword" type="password" autocomplete="off" class="form-control form-control-sm" id="newPassword" placeholder="New Password" aria-describedby="inputGroupPrepend" required>
								<div class="invalid-feedback">
									Please enter new password.
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="sr-only" for="confirmPassword">Confirm Password</label>
						<div class="input-group">
		
							<input name="confirmPassword" type="password" autocomplete="off" class="form-control form-control-sm" id="confirmPassword" placeholder="Confirm Password" aria-describedby="inputGroupPrepend" required>
							<div class="invalid-feedback">
								Password not a match.
							</div>
						</div>
					</div>
		
					<button id="submitBtn" class="btn btn-md btn-primary btn-block" type="submit">Update</button>
		
				</div>
			</div>
					
	</div>
</div>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/js/vendor.min.js"></script>

<!-- Plugins js-->
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/apexcharts/apexcharts.min.js"></script>

<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/selectize/js/standalone/selectize.min.js"></script>

<!-- Dashboar 1 init js-->
<script src="<?=base_url()?>/Assets/Admin/dist/assets/js/pages/dashboard-1.init.js"></script>

<!-- App js-->
<script src="<?=base_url()?>/Assets/Admin/dist/assets/js/app.min.js"></script>

 <!-- third party js -->
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/pdfmake.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="<?=base_url()?>/Assets/Admin/dist/assets/libs/pdfmake/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<!-- third party js ends -->

<script>
		$(document).ready(function () {
				$('#submitBtn').click(function (e) {
					var id_user = $('#id_user').val();
					var newPassword = $('#newPassword').val();
					var confirmPassword = $('#confirmPassword').val();
					if(id_user==null){
						Swal.fire(
							'Maaf',
							'Link Telah Kadaluarsa',
							'error'
						)
					}else if(newPassword!=confirmPassword){
						Swal.fire(
							'Maaf',
							'Password Tidak Sama',
							'error'
						)
					}else{
						$.ajax({
							type: "post",
							url: '<?= base_url() ?>/ResetPassword/GantiPasword',
							data: "data",
							dataType: {
								id_user,
								newPassword,
								confirmPassword
							},
							success: function (r) {
								console.log(r);
								
							}
						});
					}

					
				});

		
			});
</script>

