<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class AdminModel extends CI_Model {
                        
public function login(){
                        
                                
}
public function getAdminBy($cari = [])
{
		$this->db->from('admin');
		$this->db->where($cari);
		$sql = $this->db->get();
		return $sql->result();

}
public function getAdminByRow($cari = [])
{
	// var_dump($cari);die;
		$this->db->from('admin');
		$this->db->where($cari);
		$sql = $this->db->get();
		return $sql->row();

}
public function getAdminByExeptID($cari = [], $id_admin)
{
		$this->db->from('admin');
		$this->db->where($cari);
		$this->db->where('id_admin != ' . $id_admin);
		$sql = $this->db->get();
		return $sql->result();

}
public function tambahAdmin($data)
{
	$this->db->insert('admin', $data);
	return $this->db->insert_id();
}
public function dt_user($post)
{
	$from = 'admin a';
	$columns = array(
		'nama_admin',
		'email',
		'username',
		'role_admin'
	);
	// untuk search
	$columnsSearch = array(
		'nama_admin',
		'email',
		'username',
	);
	$sql = "  SELECT * from $from";
	$where = "";
	if (isset($post['status']) && $post['status'] != 'default') {
		if ($where != "") $where .= "AND";
		$where .= " (status='" . $post['status'] . "')";
	} 
	$whereTemp = "";
			
	if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
	else if ($whereTemp != '') $where .= $whereTemp;
	// search
	if (isset($post['search']['value']) && $post['search']['value'] != '') {
		$search = $post['search']['value'];
		$whereTemp = "";
		for ($i = 0; $i < count($columnsSearch); $i++) {
			$whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
			if ($i < count($columnsSearch) - 1) {
				$whereTemp .= ' OR ';
			}
		}
		if ($where != '') $where .= " AND (" . $whereTemp . ")";
		else $where .= $whereTemp;
	}

	if ($where != '') $sql .= ' where  (' . $where . ')';
	//SORT Kolom
	$sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
	$sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
	$sortColumn = $columns[$sortColumn - 1];
	$sql .= " ORDER BY {$sortColumn} {$sortDir}";
	// var_dump($sql);
	// die();
	$count = $this->db->query($sql);
	// hitung semua data
	$totaldata = $count->num_rows();
	// memberi Limit
	$start  = isset($post['start']) ? $post['start'] : 0;
	$length = isset($post['length']) ? $post['length'] : 10;        
	// $sql .= " LIMIT {$start}, {$length}";
	if($start==0 && $length==-1){           

	}else{
	 $sql .= " LIMIT {$start}, {$length}";

	}
	
	$data  = $this->db->query($sql);
	return array(
		'totalData' => $totaldata,
		'data' => $data,
	);
}
public function udpateAdmin($id_admin, $data)
{
	$this->db->where('id_admin', $id_admin);
	$this->db->update('admin', $data);
}
public function deleteAdmin($id)
{
	$this->db->where('id_admin', $id);
	$this->db->delete('admin');
}

                        
                            
                        
}
                        
/* End of file AdminModel.php */
    
                        