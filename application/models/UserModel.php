<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model
{

    public function getUserBy($cari = [])
    {
        $this->db->from('user');

        $this->db->where($cari);
        $sql = $this->db->get();
        return $sql->result();

    }
    public function getUserByExeptID($cari = [], $id_user)
    {
        $this->db->from('user');

        $this->db->where($cari);
        $this->db->where('id_user != ' . $id_user);
        $sql = $this->db->get();
        return $sql->result();

    }
    public function getUserByRow($cari)
    {
        $this->db->from('user');

        $this->db->where($cari);
        $sql = $this->db->get();
        return $sql->row();

    }
    public function register($data)
    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    public function udpateUser($id_user, $data)
    {
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);

        # code...
    }
    public function getUserAll()
    {
        $this->db->from('user');
        $sql = $this->db->get();
        return $sql->result();
    }
		public function dt_user($post)
    {
        $from = 'user u';
        $columns = array(
            'u.nama_user',
            'u.email',
            'u.no_phone',
        );
        // untuk search
        $columnsSearch = array(
            'nama_user',
            'email',
            'no_phone',
        );
        $sql = "  SELECT u.* from $from";
        $where = "";
        if (isset($post['status']) && $post['status'] != 'default') {
            if ($where != "") $where .= "AND";
            $where .= " (status='" . $post['status'] . "')";
        } 
        $whereTemp = "";
				
        if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
        else if ($whereTemp != '') $where .= $whereTemp;
        // search
        if (isset($post['search']['value']) && $post['search']['value'] != '') {
            $search = $post['search']['value'];
            $whereTemp = "";
            for ($i = 0; $i < count($columnsSearch); $i++) {
                $whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
                if ($i < count($columnsSearch) - 1) {
                    $whereTemp .= ' OR ';
                }
            }
            if ($where != '') $where .= " AND (" . $whereTemp . ")";
            else $where .= $whereTemp;
        }

        if ($where != '') $sql .= ' where  (' . $where . ')';
        //SORT Kolom
        $sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
        $sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
        $sortColumn = $columns[$sortColumn - 1];
        $sql .= " ORDER BY {$sortColumn} {$sortDir}";
        // var_dump($sql);
        // die();
        $count = $this->db->query($sql);
        // hitung semua data
        $totaldata = $count->num_rows();
        // memberi Limit
        $start  = isset($post['start']) ? $post['start'] : 0;
        $length = isset($post['length']) ? $post['length'] : 10;        
        // $sql .= " LIMIT {$start}, {$length}";
        if($start==0 && $length==-1){           

        }else{
         $sql .= " LIMIT {$start}, {$length}";

        }
        
        $data  = $this->db->query($sql);
        return array(
            'totalData' => $totaldata,
            'data' => $data,
        );
    }
	public function deleteUser($id)
	{
		$this->db->where('id_user', $id);
		$this->db->delete('user');
		
		# code...
	}


}

/* End of file UserModel.php */
