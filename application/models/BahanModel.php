<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BahanModel extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function dt_bahan($post)
  {
    $from = 'bahan b';
    $columns = array(
      'nama_bahan',
      'jenis',
    );
    // untuk search
    $columnsSearch = array(
      'nama_bahan',
      'jenis',
    );
    $sql = "  SELECT * from $from";
    $where = "";
    if (isset($post['jenis']) && $post['jenis'] != 'default') {
      if ($where != "") $where .= "AND";
      $where .= " (jenis='" . $post['jenis'] . "')";
    }
    $whereTemp = "";

    if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
    else if ($whereTemp != '') $where .= $whereTemp;
    // search
    if (isset($post['search']['value']) && $post['search']['value'] != '') {
      $search = $post['search']['value'];
      $whereTemp = "";
      for ($i = 0; $i < count($columnsSearch); $i++) {
        $whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
        if ($i < count($columnsSearch) - 1) {
          $whereTemp .= ' OR ';
        }
      }
      if ($where != '') $where .= " AND (" . $whereTemp . ")";
      else $where .= $whereTemp;
    }

    if ($where != '') $sql .= ' where  (' . $where . ')';
    //SORT Kolom
    $sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
    $sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
    $sortColumn = $columns[$sortColumn - 1];
    $sql .= " ORDER BY {$sortColumn} {$sortDir}";
    // var_dump($sql);
    // die();
    $count = $this->db->query($sql);
    // hitung semua data
    $totaldata = $count->num_rows();
    // memberi Limit
    $start  = isset($post['start']) ? $post['start'] : 0;
    $length = isset($post['length']) ? $post['length'] : 10;
    // $sql .= " LIMIT {$start}, {$length}";
    if ($start == 0 && $length == -1) {
    } else {
      $sql .= " LIMIT {$start}, {$length}";
    }

    $data  = $this->db->query($sql);
    return array(
      'totalData' => $totaldata,
      'data' => $data,
    );
  }
  public function tambahBahan($data)
  {

    $this->db->insert('bahan', $data);
    return $this->db->insert_id();
  }
  public function deleteBahan($id)
  {
    $this->db->where('id_bahan', $id);
    $this->db->delete('bahan');
    # code...
  }
  public function udpateBahan($id_bahan, $data)
  {
    $this->db->where('id_bahan', $id_bahan);
    $this->db->update('bahan', $data);

    # code...
  }
  public function getDataByRow($cari = [])
  {
    // var_dump($cari);die;
    $this->db->from('bahan');
    $this->db->where($cari);
    $sql = $this->db->get();
    return $sql->row();
  }
}
