<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BannerModel extends CI_Model
{

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function index()
  {
    $this->db->from('banner');

    $this->db->where('status', 'aktive');

    $s = $this->db->get();
    return $s->result();
  }
  public function dt_banner($post)
  {
    $from = 'banner a';
    $columns = array(
      'judul',
      'sub_judul',
      'deskripsi',
      'foto',
      'status'
    );
    // untuk search
    $columnsSearch = array(
      'judul',
      'sub_judul',
      'deskripsi',
      'status'
    );
    $sql = "  SELECT * from $from";
    $where = "";
    if (isset($post['status']) && $post['status'] != 'default') {
      if ($where != "") $where .= "AND";
      $where .= " (status='" . $post['status'] . "')";
    }
    $whereTemp = "";

    if ($whereTemp != '' && $where != '') $where .= " AND (" . $whereTemp . ")";
    else if ($whereTemp != '') $where .= $whereTemp;
    // search
    if (isset($post['search']['value']) && $post['search']['value'] != '') {
      $search = $post['search']['value'];
      $whereTemp = "";
      for ($i = 0; $i < count($columnsSearch); $i++) {
        $whereTemp .= $columnsSearch[$i] . ' LIKE "%' . $search . '%"';
        if ($i < count($columnsSearch) - 1) {
          $whereTemp .= ' OR ';
        }
      }
      if ($where != '') $where .= " AND (" . $whereTemp . ")";
      else $where .= $whereTemp;
    }

    if ($where != '') $sql .= ' where  (' . $where . ')';
    //SORT Kolom
    $sortColumn = isset($post['order'][0]['column']) ? $post['order'][0]['column'] : 1;
    $sortDir    = isset($post['order'][0]['dir']) ? $post['order'][0]['dir'] : 'asc';
    $sortColumn = $columns[$sortColumn - 1];
    $sql .= " ORDER BY {$sortColumn} {$sortDir}";
    // var_dump($sql);
    // die();
    $count = $this->db->query($sql);
    // hitung semua data
    $totaldata = $count->num_rows();
    // memberi Limit
    $start  = isset($post['start']) ? $post['start'] : 0;
    $length = isset($post['length']) ? $post['length'] : 10;
    // $sql .= " LIMIT {$start}, {$length}";
    if ($start == 0 && $length == -1) {
    } else {
      $sql .= " LIMIT {$start}, {$length}";
    }

    $data  = $this->db->query($sql);
    return array(
      'totalData' => $totaldata,
      'data' => $data,
    );
  }
  public function tambahBanner($data)
{
		$this->db->insert('banner', $data);
		return $this->db->insert_id();
}
public function getDataByRow($cari = [])
{
	// var_dump($cari);die;
		$this->db->from('banner');
		$this->db->where($cari);
		$sql = $this->db->get();
		return $sql->row();
}
public function deleteBanner($id)
{
	$this->db->where('id_banner', $id);
	$this->db->delete('banner');	
	# code...
}
public function udpateBanner($id_banner, $data)
{
	$this->db->where('id_banner', $id_banner);
	$this->db->update('banner', $data);

	# code...
}
  
}
