<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('AdminModel');
        $this->load->model('BannerModel');
        $this->load->model('BahanModel');
    }
    public function bizEncrypt($plaintext)
    {
        $tahun = date('Y');
        $bulan = date('m');
        $hari = date('d');
        $jam = date('H');
        $menit = date('i');
        $detik = date('s');
        $pool = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_+&';
        $word1 = '';
        for ($i = 0; $i < 4; $i++) {
            $word1 .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
        }
        $plain = $hari . $bulan . $tahun . $word1 . base64_encode(base64_encode($plaintext)) . $detik . $menit . $jam;
        $enc = base64_encode($plain);
        return $enc;
    }
    public function bizDecrypt($enc)
    {
        $dec64 = base64_decode($enc);
        $substr1 = substr($dec64, 12, strlen($dec64) - 12);
        $substr2 = substr($substr1, 0, strlen($substr1) - 6);
        $dec = base64_decode(base64_decode($substr2));
        return $dec;
    }
    public function index()
    {
        $this->cekLoginAdmin();
        $data['dataUser'] = $this->UserModel->getUserAll();
        $this->load->view('Template',$data);
    }
    public function master_admin()
    {
        $this->cekLoginAdmin();
        $this->load->view('Master_Admin');
    }
    public function banner()
    {
        $this->cekLoginAdmin();
        $this->load->view('Admin/Master_Banner');
    }
    public function bahan()
    {
        $this->cekLoginAdmin();
        $this->load->view('Admin/Master_bahan');
    }
    public function master_user()
    {
        $this->cekLoginAdmin();
        $obj['dataUser'] = $this->UserModel->getUserBy();
        $obj['ci'] = $this;
        $this->load->view('Master_user', $obj);
    }
    public function profile()
    {
        $this->cekLoginAdmin();
        $id_admin = $_SESSION['id_admin'];
        $obj['dataAdmin'] = $this->AdminModel->getAdminByRow(array('id_admin' => $id_admin));
        $obj['dataUser'] = $this->UserModel->getUserBy();
        $this->load->view('Admin/Profile', $obj);
    }
    public function UpdateProfile()
    {
        $id_admin = $_SESSION['id_admin'];
        $nama_admin = $this->input->post('nama_admin');
        $email = $this->input->post('email');
        $no_phone = $this->input->post('no_phone');
        $dataEror = [];
        $error = false;

        if ($nama_admin == null || $email == null || $no_phone == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {

            $cekEmail = $this->AdminModel->getAdminByExeptID(array('email' => $email), $id_admin);
            $cekno_phone = $this->AdminModel->getAdminByExeptID(array('no_phone' => $no_phone), $id_admin);

            if ($cekEmail != null) {
                $error = true;
                $dataEror += ['email' => 'Email Sudah Ada!'];
            }
            if ($cekno_phone != null) {
                $error = true;
                $dataEror += ['no_phone' => 'Nomor Telpon Sudah Ada!'];
            }
            if ($error) {
                $this->res(null, $dataEror, 401);
            } else {
                $dataSimpan = array(
                    'nama_admin' => $nama_admin,
                    'email' => $email,
                    'no_phone' => $no_phone,
                );
                $this->AdminModel->udpateAdmin($id_admin, $dataSimpan);

                $this->res(null, 'Data Berhasil Di Ubah!', 200);
            }
        }
    }
    public function HapusUser()
    {
        $id_user = $this->input->post('id_user');
        $cek = $this->UserModel->getUserByRow(array('id_user' => $id_user));
        if ($cek == null) {
            $this->res(null, 'Data Bermasalah, Coba Lagi Nanti', 401);
        } else {
            $this->UserModel->deleteUser($id_user);
            $this->res(null, 'Data Berhasil Dihapus', 200);
        }
    }
    public function EditUser()
    {
        $id_user = $this->input->post('id_user');
        $nama_user = $this->input->post('nama_user');
        $email = $this->input->post('email');
        $no_phone = $this->input->post('no_phone');
        $dataEror = [];
        $error = false;

        if ($nama_user == null || $email == null || $no_phone == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {

            $cekEmail = $this->UserModel->getUserByExeptID(array('email' => $email), $id_user);
            $cekno_phone = $this->UserModel->getUserByExeptID(array('no_phone' => $no_phone), $id_user);

            if ($cekEmail != null) {
                $error = true;
                $dataEror += ['email' => 'Email Sudah Ada!'];
            }
            if ($cekno_phone != null) {
                $error = true;
                $dataEror += ['no_phone' => 'Nomor Telpon Sudah Ada!'];
            }
            if ($error) {
                $this->res(null, $dataEror, 401);
            } else {
                $dataSimpan = array(
                    'nama_user' => $nama_user,
                    'email' => $email,
                    'no_phone' => $no_phone,
                    // 'foto_user' => $foto_user,
                    // 'password' => md5($password),
                );
                $this->UserModel->udpateUser($id_user, $dataSimpan);

                $this->res(null, 'Data Berhasil Di Ubah!', 200);
            }
        }

        $cek = $this->UserModel->getUserByRow(array('id_user' => $id_user));
    }
    public function tambah_admin()
    {
        $nama_admin = $this->input->post('nama_admin');
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $no_phone = $this->input->post('no_phone');
        $password = $this->input->post('password');
        $role_admin = $this->input->post('role_admin');
        $dataEror = [];
        $error = false;
        if ($nama_admin == '' || $email == '' || $username == '' || $no_phone == '' || $password == '' || $role_admin == '') {
            $this->res(null, 'Harap Isi Data Yang Di Butuhkan!', 401);
            // die;
        } else {
            $cekEmail = $this->AdminModel->getAdminBy(array('email' => $email));
            $CekUsername = $this->AdminModel->getAdminBy(array('username' => $username));
            $Cekno_phone = $this->AdminModel->getAdminBy(array('no_phone' => $no_phone));
            if ($cekEmail != null) {
                $error = true;
                $dataEror += ['email' => 'Email Sudah Ada!'];
            }
            if ($Cekno_phone != null) {
                $error = true;
                $dataEror += ['no_phone' => 'no_phone Sudah Ada!'];
            }
            if ($CekUsername != null) {
                $error = true;
                $dataEror += ['username' => 'Nomor Telpon Sudah Ada!'];
            }
            if ($error) {
                $this->res(null, $dataEror, 401);
            } else {
                $dataSimpan = array(
                    'nama_admin' => $nama_admin,
                    'username' => $username,
                    'email' => $email,
                    'role_admin' => $role_admin,
                    'password' => $this->bizEncrypt($password),
                );
                $save = $this->AdminModel->tambahAdmin($dataSimpan);
                $this->res(null, 'Data Berhasil Di simpan', 200);
            }
        }
    }
    public function login()
    {
        $this->load->view('Admin/Login');
        # code...
    }
    public function passwordMatch($plain_password, $encrypted)
    {
        return $plain_password == $this->bizDecrypt($encrypted);
    }
    public function loginAdmin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // var_dump($password);
        // var_dump($this->bizEncrypt($password));die;

        $dataEror = [];
        $error = false;

        if ($username == null || $password == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            $cek = $this->AdminModel->getAdminByRow(array('username' => $username));
            if ($cek == null) {
                $this->res(null, 'Username Tidak DiTemukan!', 401);
            } else {
                if ($this->passwordMatch($password, $cek->password)) {
                    $session = array(
                        'admin_session' => true, // Buat session authenticated dengan value true
                        'id_admin' => $cek->id_admin, // Buat session authenticated
                        'nama_admin' => $cek->nama_admin, // Buat session authenticated
                    );
                    $this->session->set_userdata($session); // Buat session sesuai $session
                    $this->res($cek, 'Berhasil Login!', 200);
                } else {
                    $this->res(null, 'Username Dan Password Tidak Sama!', 401);
                }
            }
        }

        # code...
    }
    public function GetDataAdmin()
    {
        $bu = base_url();
        $dt = $this->AdminModel->dt_user($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $v) {

            $fields = array($no++);
            $fields[] = $v->nama_admin;
            $fields[] = $v->username;
            $fields[] = $v->email;
            $fields[] = $v->role_admin;

            $fields[] = '
				<button class="btn btn-warning Edit btn-block"
				data-id_admin="' . $v->id_admin . '"
				data-nama_admin="' . $v->nama_admin . '"
				data-email="' . $v->email . '"
				data-username="' . $v->username . '"
				data-no_phone="' . $v->no_phone . '"
				data-password="' . $this->bizDecrypt($v->password) . '"

				type="button">Edit</button>
				<button class="btn btn-danger Hapus btn-block"
				data-id_admin="' . $v->id_admin . '"
				data-nama_admin="' . $v->nama_admin . '"
				data-username="' . $v->username . '"
				data-role_admin="' . $v->role_admin . '"

				type="button">Hapus</button>

						';

            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();

        // var_dump($datatable);die;
    }
    public function EditAdmin()
    {
        $id_admin = $this->input->post('id_admin');
        $nama_admin = $this->input->post('nama_admin');
        $email = $this->input->post('email');
        $no_phone = $this->input->post('no_phone');
        $password = $this->input->post('password');
        $role = $this->input->post('role');
        $username = $this->input->post('username');
        $dataEror = [];
        $error = false;

        if ($nama_admin == null || $email == null || $no_phone == null || $password == null || $username == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {

            $cekEmail = $this->AdminModel->getAdminByExeptID(array('email' => $email), $id_admin);
            $cekno_phone = $this->AdminModel->getAdminByExeptID(array('no_phone' => $no_phone), $id_admin);

            if ($cekEmail != null) {
                $error = true;
                $dataEror += ['email' => 'Email Sudah Ada!'];
            }
            if ($cekno_phone != null) {
                $error = true;
                $dataEror += ['no_phone' => 'Nomor Telpon Sudah Ada!'];
            }
            if ($error) {
                $this->res(null, $dataEror, 401);
            } else {
                $dataSimpan = array(
                    'nama_admin' => $nama_admin,
                    'email' => $email,
                    'no_phone' => $no_phone,
                    // 'foto_user' => $foto_user,
                    'password' => $this->bizEncrypt($password),
                    'role_admin' => $role,
                    'username' => $username,
                );
                $this->AdminModel->udpateAdmin($id_admin, $dataSimpan);

                $this->res(null, 'Data Berhasil Di Ubah!', 200);
            }
        }

        // $cek = $this->UserModel->getUserByRow(array('id_admin' => $id_admin));
    }
    public function HapusAdmin()
    {
        $id_admin = $this->input->post('id_admin');
        $cek = $this->AdminModel->getAdminByRow(array('id_admin' => $id_admin));
        if ($cek == null) {
            $this->res(null, 'Data Bermasalah, Coba Lagi Nanti', 401);
        } else {
            $this->AdminModel->deleteAdmin($id_admin);
            $this->res(null, 'Data Berhasil Dihapus', 200);
        }
    }
    public function logout()
    {
        $this->logoutAdmin();
        $this->res(null, 'Berhasil Keluar', 200);
        # code...
    }
    public function EditPassword()
    {
        $id_admin = $_SESSION['id_admin'];

        $password = $this->input->post('password');
        $re_password = $this->input->post('re_password');
        if ($password != $re_password) {
            $this->res(null, 'Password Tidak Sama!', 401);
            die;
        }

        $dataEror = [];
        $error = false;

        if ($password == null || $re_password == null) {
            $error = true;
            $this->res(null, 'Silahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            if ($error) {
                $this->res(null, 'Gagal, Coba Lagi Nanti', 401);
            } else {
                $dataSimpan = array(
                    'password' => $this->bizEncrypt($password),
                );
                $this->AdminModel->udpateAdmin($id_admin, $dataSimpan);

                $this->res(null, 'Data Berhasil Di Ubah!', 200);
            }
        }

        // $cek = $this->UserModel->getUserByRow(array('id_admin' => $id_admin));
    }
    public function updateFotoProfile()
    {
        $id_admin = $_SESSION['id_admin'];
        $config['upload_path'] = './Images/foto_admin/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 100 * 100;
        $config['max_width'] = 1024 * 100;
        $config['max_height'] = 768 * 100;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $this->res(null, $this->upload->display_errors(), 401);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $dataSimpan = array(
                'foto_admin' => $data['upload_data']['file_name'],
            );
            $this->AdminModel->udpateAdmin($id_admin, $dataSimpan);
            $this->res(null, 'Data Berhasil Di Ubah!', 200);
        }
    }
    public function GetBannerAdmin()
    {
        $bu = base_url();
        $dt = $this->BannerModel->dt_banner($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $v) {

            $fields = array($no++);
            $fields[] = $v->judul;
            $fields[] = $v->sub_judul;
            $fields[] = $v->deskripsi;
            $fields[] = $v->status;
            // $fields[] = $v->foto;
            $fields[] = '<img src="' . base_url() . 'Images/banner/' . $v->foto . '" class="img-fluid" alt="Responsive image">';

            $fields[] = '
				<button class="btn btn-warning Edit btn-block"
				data-id_banner="' . $v->id_banner . '"
				data-judul="' . $v->judul . '"

				type="button">Edit</button>
                
				<button class="btn btn-danger Hapus btn-block"
				data-id_banner="' . $v->id_banner . '"
				data-judul="' . $v->judul . '"
				type="button">Hapus</button>
						';
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();

        // var_dump($datatable);die;
    }
    public function addBanner()
    {
        $judul = $this->input->post('judul');
        $sub_judul = $this->input->post('sub_judul');
        $deskripsi = $this->input->post('deskripsi');
        $status = $this->input->post('status');
        $gambar = $_FILES['gambar'];
        if ($judul == null || $sub_judul == null || $deskripsi == null || $status == null || $gambar == null) {
            $this->res(null, 'Harap lengkapi Data Yang Di Butuhkan!', 401);
        } else {

            $config['upload_path'] = './Images/banner/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 100 * 100;
            $config['max_width'] = 1024 * 100;
            $config['max_height'] = 768 * 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar')) {
                $this->res(null, $this->upload->display_errors(), 401);
            } else {
                $data = array('upload_data' => $this->upload->data());
                $dataSimpan = array(
                    'foto' => $data['upload_data']['file_name'],
                    'judul' => $judul,
                    'sub_judul' => $sub_judul,
                    'deskripsi' => $deskripsi,
                    'status' => $status,
                );
                $this->BannerModel->tambahBanner($dataSimpan);
                $this->res(null, 'Data Berhasil Di Simpan!', 200);
            }
        }
    }
    public function HapusBanner()
    {
        $id_banner = $this->input->post('id_banner');
        $cek = $this->BannerModel->getDataByRow(array('id_banner' => $id_banner));
        if ($cek == null) {
            $this->res(null, 'Data Bermasalah, Coba Lagi Nanti', 401);
        } else {
            $this->BannerModel->deleteBanner($id_banner);
            $this->res(null, 'Data Berhasil Dihapus', 200);
        }
    }
    public function GetBannerByID()
    {
        $id_banner = $this->input->post('id_banner');
        $cek = $this->BannerModel->getDataByRow(array('id_banner' => $id_banner));

        if ($cek == null) {
            $this->res(null, 'Data Bermasalah, Coba Lagi Nanti', 401);
        } else {
            $this->res($cek, 'Data Berhasil Dihapus', 200);
        }
    }
    public function editBanner()
    {
        $id_banner = $this->input->post('id_banner');
        $judul = $this->input->post('judul');
        $sub_judul = $this->input->post('sub_judul');
        $deskripsi = $this->input->post('deskripsi');
        $status = $this->input->post('status');
        $gambar = $_FILES['gambar'];
        if ($judul == null || $sub_judul == null || $deskripsi == null || $status == null || $gambar == null) {
            $this->res(null, 'Harap lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            $dataSimpan = array(
                'judul' => $judul,
                'sub_judul' => $sub_judul,
                'deskripsi' => $deskripsi,
                'status' => $status,
            );
            if ($gambar['name'] != null) {
                $config['upload_path'] = './Images/banner/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 100 * 100;
                $config['max_width'] = 1024 * 100;
                $config['max_height'] = 768 * 100;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('gambar')) {
                    $this->res(null, $this->upload->display_errors(), 401);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $dataSimpan += array(
                        'foto' => $data['upload_data']['file_name'],
                    );
                    $this->BannerModel->udpateBanner($id_banner, $dataSimpan);
                    $this->res(null, 'Data Berhasil Di Ubah!', 200);
                }
            } else {
                $this->BannerModel->udpateBanner($id_banner, $dataSimpan);
                $this->res(null, 'Data Berhasil Di Ubah!', 200);
            }
        }
    } 
    public function GetBahan()
    {
        $bu = base_url();
        $dt = $this->BahanModel->dt_bahan($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $v) {

            $fields = array($no++);
            $fields[] = $v->nama_bahan;
            $fields[] = $v->jenis;
            $fields[] = '
				<button class="btn btn-warning Edit btn-block"
				data-id_bahan="' . $v->id_bahan . '"
				data-nama_bahan="' . $v->nama_bahan . '"
				data-jenis="' . $v->jenis . '"

				type="button">Edit</button>
                
				<button class="btn btn-danger Hapus btn-block"
				data-id_bahan="' . $v->id_bahan . '"
				data-nama_bahan="' . $v->nama_bahan . '"
				data-jenis="' . $v->jenis . '"
				type="button">Hapus</button>
						';
            $datatable['data'][] = $fields;
        }
        echo json_encode($datatable);
        exit();

        // var_dump($datatable);die;
    }
    public function addBahan()
    {
        $nama_bahan = $this->input->post('nama_bahan');
        $jenis = $this->input->post('jenis');
        if($nama_bahan==null||$jenis==null){
            $this->res(null, 'Harap lengkapi Data Yang Di Butuhkan!', 401);
        }else{
            $data = array(
                'nama_bahan' => $nama_bahan,
                'jenis' => $jenis,
             );
             $this->BahanModel->tambahBahan($data);
             $this->res(null, 'Data Berhasil Di Simpan!', 200);
        }
        
        # code...
    }
    public function editBahan()
    {
        $id_bahan = $this->input->post('id_bahan');
        $nama_bahan = $this->input->post('nama_bahan');
        $jenis = $this->input->post('jenis');
        if ($nama_bahan == null || $jenis == null ) {
            $this->res(null, 'Harap lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            $dataSimpan = array(
                'nama_bahan' => $nama_bahan,
                'jenis' => $jenis,
            );
            $this->BahanModel->udpateBahan($id_bahan, $dataSimpan);
            $this->res(null, 'Berhasil Mengubah Bahan!', 200);
            
        }
    }
    public function HapusBahan()
    {
        $id_bahan = $this->input->post('id_bahan');
        $cek = $this->BahanModel->getDataByRow(array('id_bahan' => $id_bahan));
        if ($cek == null) {
            $this->res(null, 'Data Bermasalah, Coba Lagi Nanti', 401);
        } else {
            $this->BahanModel->deleteBahan($id_bahan);
            $this->res(null, 'Data Berhasil Dihapus', 200);
        }
    }
}
