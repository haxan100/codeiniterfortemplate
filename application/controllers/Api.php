<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('UserModel');
        $this->load->model('BannerModel');
        $this->load->library('upload');
    }

    public function register()
    {
        $nama_user = $this->input->post('nama_user');
        $email = $this->input->post('email');
        $no_phone = $this->input->post('no_phone');
        $password = $this->input->post('password');
        $foto_user = $this->input->post('foto_user');

        $cekEmail = $this->UserModel->getUserBy(array('email' => $email));
        $cekno_phone = $this->UserModel->getUserBy(array('no_phone' => $no_phone));
        $dataEror = [];
        $error = false;

        if ($nama_user == null || $email == null || $no_phone == null || $password == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            if ($cekEmail != null) {
                $error = true;
                $dataEror += ['email' => 'Email Sudah Ada!'];
            }
            if ($cekno_phone != null) {
                $error = true;
                $dataEror += ['no_phone' => 'Nomor Telpon Sudah Ada!'];
            }
            if ($error) {
                $this->res(null, $dataEror, 401);
            } else {
                $dataSimpan = array(
                    'nama_user' => $nama_user,
                    'email' => $email,
                    'no_phone' => $no_phone,
                    'foto_user' => $foto_user,
                    'password' => md5($password),
                );
                $save = $this->UserModel->register($dataSimpan);
                $this->res(null, 'Data Berhasil Di simpan, Silahkan Login Untuk Masuk Ke Halaman!', 200);
            }
        }
    }
    public function loginbyEmail()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $dataEror = [];
        $error = false;

        if ($email == null ||  $password == null) {
            $error = true;
            $this->res(null, 'SIlahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            $cek = $this->UserModel->getUserByRow(array('email' => $email, 'password' => md5($password)));
            if ($cek == null) {
                $this->res(null, 'Email Dan Password Tidak Sesuai!', 401);
            } else {
                $session = array(
                    'user_session' => true, // Buat session authenticated dengan value true
                    'id_user' => $cek->id_user, // Buat session authenticated
                );
                $this->session->set_userdata($session); // Buat session sesuai $session
                $this->res($cek, 'Berhasil Login!', 200);
            }
        }
    }
    public function loginbyNoPhone()
    {
        $no_phone = $this->input->post('no_phone');
        $password = $this->input->post('password');
        $pertama = $no_phone[0];

        $dataEror = [];
        $error = false;

        if ($no_phone == null ||  $password == null) {
            $error = true;
            $this->res(null, 'Silahkan Lengkapi Data Yang Di Butuhkan!', 401);
        } else {
            if ($pertama != 8 || $pertama != '8') {
                $this->res(null, 'Harap Masukan Nomor Telpon Yang Sesuai !', 401);
                die;
            }
            $cek = $this->UserModel->getUserByRow(array('no_phone' => $no_phone, 'password' => md5($password)));
            if ($cek == null) {
                $this->res(null, 'Nomor Telpon Dan Password Tidak Sesuai!', 401);
            } else {
                $session = array(
                    'user_session' => true, // Buat session authenticated dengan value true
                    'id_user' => $cek->id_user, // Buat session authenticated
                );
                $this->session->set_userdata($session); // Buat session sesuai $session
                $this->res($cek, 'Berhasil Login!', 200);
            }
        }
    }
    public function cekLogin()
    {

        var_dump($_SESSION);
        die;
    }
    public function updatePassword()
    {
        $id_user = $_SESSION['id_user'];
        $password = $this->input->post('password');
        $passwordNew = $this->input->post('passwordNew');
        $RepasswordNew = $this->input->post('RepasswordNew');
        $cek = $this->UserModel->getUserByRow(array('id_user' => $id_user, 'password' => md5($password)));
        if ($cek == null) {
            $this->res(null, 'Password Salah!', 401);
        } else if ($passwordNew != $RepasswordNew) {
            $this->res(null, 'Password Tidak Sama!!', 401);
        } else {
            $data = array('password' => md5($passwordNew));
            $this->UserModel->udpateUser($id_user, $data);
            $this->res(null, 'Password Berhasil Diupdate', 200);
        }
    }
    public function checkIsLogin()
    {
        $cek = $this->session->userdata('user_session');
        if ($cek != true) {
            $this->res(null, 'Harap Login Terlebih Dahulu', 401);
        }
    }
    public function updateFotoProfile()
    {
        $this->checkIsLogin();
        $id_user = $_SESSION['id_user'];
        $foto_user = $this->input->post('foto_user');

        $config['upload_path'] = './Images/foto_profile'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] = $id_user.'_'.date('dmY'); //Enkripsi nama yang terupload
        $config['file_ext'] = '.jpg'; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if (!empty($_FILES['foto_user']['name'])) {
            if (!$this->upload->do_upload('foto_user')) {
                $this->res(null,$this->upload->display_errors(),401);
            } else {                
                $data = array('foto_user' => $config['file_name'].$config['file_ext']);
                $this->UserModel->udpateUser($id_user, $data);
                $this->res(null,'Berhasil Update Foto Profile',200);
            }
        } else {
            $this->res(null,'Image yang diupload kosong',401);
        }
    }
    public function updateProfile()
    {
        $this->checkIsLogin();
        $id_user = $_SESSION['id_user'];
        $nama_user = $this->input->post('nama_user');
        $email = $this->input->post('email');
        $no_phone = $this->input->post('no_phone');

        $cek = $this->UserModel->getUserByRow(array('id_user' => $id_user));
        if ($cek == null) {
            $this->res(null, 'Tidak Ada User, Mohon Coba Lagi Nanti', 401);
        } else if ($nama_user==null || $email==null ||$no_phone==null) {
            $this->res(null, 'Mohon Masukan Semua Data', 401);
        } else {
            $data = array(
                'nama_user' => $nama_user,
                'email' => $email,
                'no_phone' => $no_phone,
            );
            $this->UserModel->udpateUser($id_user, $data);
            $this->res(null, 'Data Berhasil Diupdate', 200);
        }
    }
    public function banner()
    {
        $data = $this->BannerModel->index();
        $newData = [];
        foreach ($data as $da => $dax) {
            $newData[] = [
                'id_banner'=>$dax->id_banner,
                'judul'=>$dax->judul,
                'sub_judul'=>$dax->sub_judul,
                'deskripsi'=>$dax->deskripsi,
                'foto'=>base_url().'/Images/banner/'.$dax->foto,
                'status'=>$dax->status,
            ];
        }
        $this->res($newData, 'Data', 200);
    }
}
