<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');

    }
    public function GetDataUser()
    {
        $bu = base_url();
        $dt = $this->UserModel->dt_user($_POST);
        $datatable['draw'] = isset($_POST['draw']) ? $_POST['draw'] : 1;
        $datatable['recordsTotal'] = $dt['totalData'];
        $datatable['recordsFiltered'] = $dt['totalData'];
        $datatable['data'] = array();
        $start = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = $start + 1;
        foreach ($dt['data']->result() as $v) {

            $fields = array($no++);
            $fields[] = $v->nama_user;
            $fields[] = $v->email;
            $fields[] = $v->no_phone;
           
						$fields[] = '
						<button class="btn btn-warning Edit btn-block" 
						data-id_user="'.$v->id_user.'"
						data-nama_user="'.$v->nama_user.'"  
						data-email="'.$v->email.'"  
						data-no_phone="'.$v->no_phone.'"  
						data-status="'.$v->status.'"  
						data-foto_user="'.$v->foto_user.'"  
					
						type="button">Edit</button> 
					<button class="btn btn-danger Hapus btn-block" 
					data-id_user="'.$v->id_user.'"
					data-nama_user="'.$v->nama_user.'"  
					data-email="'.$v->email.'"  
					data-no_phone="'.$v->no_phone.'"  
					data-status="'.$v->status.'"  
					data-foto_user="'.$v->foto_user.'"  
					type="button">Hapus</button>
						
						';

						$datatable['data'][] = $fields;
					}
					echo json_encode($datatable);
					exit();

					// var_dump($datatable);die;
    }

}
