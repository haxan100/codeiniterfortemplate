<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function namaAplikasi()
    {
        return 'I Print';
    }
	public function res($data=null,$m=null,$code=401)
	{
		$status = false;
		if($code==200 || $code=='200'){
			$status = true;
		}
		$res = array(
			'message' => $m,
			'code' => $code,
			'status' => $status,
			'data' => $data,
		 );
		echo json_encode($res);
	}
	public function cekLoginAdmin()
	{
		if( !isset($_SESSION["admin_session"]) || $_SESSION["admin_session"] ==false ){
			header("location:login");
			exit();
		}
	}
	public function logoutAdmin()
	{
		session_destroy();
		// if(session_destroy()){
		// 	header("location:login");
		// 	exit();
		// }
	}

}
